# krand
Small utility pseudo random number generator library built with xoshiro256++/xoshiro256** generators.
Additional features include some distributions and mt19937 PRNG (disabled by default).

## NOTE
- This has only been tested only on systems with a GCC or Clang compiler.
- This library is NOT meant for cryptographic usage.

## Installation
```bash
# build
# check meson_options.txt and include -Doption=value if necessary
# example:
# meson setup build -Dwith_dist=true
meson setup build


# compile
cd build
ninja

# install
ninja install

# uninstall
ninja uninstall
```

## Documentation
Check `krand.h`

## Version
1.6

## License
MIT
