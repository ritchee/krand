#include "krand.h"

__thread kstate _ds = {
	{
		15825392874305938225ull,
		16955680563785373609ull,
		7795344188541920541ull,
		15657919877309493704ull
	}
};

__thread kstate32 _ds32 = {
	{
    1777456429,
    3007012567,
    4219636203,
    1433776259
	}
};

__thread kstate _dsf = {
	{
		16955680563785373609ull,
		7795344188541920541ull,
		15657919877309493704ull,
		15825392874305938225ull
	}
};

static u64 
splitmix64(u64 *s) {
	u64 r;

	r = (*s += 0x9e3779b97f4a7c15);
	r = (r ^ (r >> 30)) * 0xbf58476d1ce4e5b9;
	r = (r ^ (r >> 27)) * 0x94d049bb133111eb;
	r ^= (r >> 31);

	return r;
}

void 
kseed_r(kstate* s, u64 n)
{
	s->s[0] = splitmix64(&n);
	s->s[1] = splitmix64(&n);
	s->s[2] = splitmix64(&n);
	s->s[3] = splitmix64(&n);
}

void 
kseed32_r(kstate32* s, u32 n)
{
	u64 _s;

	_s = n;
	u64 t = splitmix64(&_s); 
	s->s[0] = (u32)t;
	s->s[1] = (u32)(t >> 32);
	t = splitmix64(&_s); 
	s->s[0] = (u32)t;
	s->s[1] = (u32)(t >> 32);
}

u64 
krand_r(kstate* s)
{
	u64 r = lrot64(s->s[1] * 5, 7) * 9;
	u64 t = s->s[1] << 17;

	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];

	s->s[2] ^= t;
	s->s[3] = lrot64(s->s[3], 45);

	return r;
}

u64
krand_range_r(kstate* s, u64 h)
{
	u64 t, x;

	// if h is a power of 2
	if ((h & (h - 1)) == 0)
		return krand_r(s) & (h - 1);

	// take top rejection range instead of bottom
	t = (u64)-1 - (-h % h);

	do {
		x = krand_r(s);
	} while (x > t);

	return x % h;
}

u32 
krand32_r(kstate32* s)
{
	u32 r = lrot32(s->s[1] * 5, 7) * 9;
	u32 t = s->s[1] << 9;

	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];

	s->s[2] ^= t;

	s->s[3] = lrot32(s->s[3], 11);

	return r;
}

u32 
krand32_range_r(kstate32* s, u32 r)
{
	u32 t, x, l;
	u64 m;

	/* integer method */
	t = -r % r;

	do {
		x = krand32_r(s);
		m = (u64)x * (u64)r;
		l = (u32)m;
	} while (l < t);

	return m >> 32;
}

void
kseed(u64 n)
{
    kseed_r(&_ds, n);
    kseed_r(&_dsf, n + 4118897797544179723ull);
}

void
kseed32(u32 n)
{
    kseed32_r(&_ds32, n);
}

u64 
krand(void)
{
    return krand_r(&_ds);
}

u64 
krand_range(u64 h)
{
    return krand_range_r(&_ds, h);
}

u32 
krand32(void)
{
    return krand32_r(&_ds32);
}

u32 
krand32_range(u32 h)
{
    return krand32_range_r(&_ds32, h);
}

double
krandf_r(kstate* s)
{
#ifdef KRAND_ONE_RNG
	return (double)krand_r(s) * 0x1.0p-64;
#else
	u64 r = s->s[0] + s->s[3];
	u64 t = s->s[1] << 17;

	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];

	s->s[2] ^= t;
	s->s[3] = lrot64(s->s[3], 45);

	return (r >> 11) * 0x1.0p-53;
#endif /* KRAND_ONE_RNG */
}

double 
krandf(void)
{
#ifdef KRAND_ONE_RNG
	return (double)krand() * 0x1.0p-64;
#else
	kstate *s = &_dsf;
	u64 r = s->s[0] + s->s[3];
	u64 t = s->s[1] << 17;

	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];

	s->s[2] ^= t;
	s->s[3] = lrot64(s->s[3], 45);

	return (r >> 11) * 0x1.0p-53;
#endif /* KRAND_ONE_RNG */
}


