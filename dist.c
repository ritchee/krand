#include "krand.h"
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif /* M_PI */

extern __thread kstate _ds;
extern __thread kstate _ds32;
extern __thread kstate _dsf;

int
kbernoulli_r(kstate *s, double p)
{
	if (p < 0.0)
		p = 0.0;
	if (p > 1.0)
		p = 1.0;

	return krandf_r(s) < p;
}

int
kbernoulli(double p)
{
	return kbernoulli_r(&_dsf, p);
}

double
kchisq_r(kstate *s, int k)
{
	double x, sum;
	int i;

	sum = 0.0;
	for (i = 0; i < k; i++) {
		x = knormal_r(s, 0.0, 1.0);
		sum += x * x;
	}

	return sum;
}

double
knormal_r(kstate *s, double mean, double sd)
{
	double u1 = krandf_r(s);
	double u2 = krandf_r(s);
	double z = sqrt(-2.0 * log(u1)) * cos(2.0 * M_PI * u2);

	return mean + sd * z;
}

double
kexponential_r(kstate *s, double lambda)
{
	double u = krandf_r(s);

	return -log(u) / lambda;
}

int
kpoisson_r(kstate *s, double lambda)
{
	double u, p, L;
	int k = 0;

	p = 1.0;
	L = exp(-lambda);
	while (p > L) {
		k++;
		u = krandf_r(s);
		p *= u;
	}

	return k - 1;
}

double
kgamma_r(kstate *s, double alpha, double beta)
{
	double u, d, c, z, v, x;

	if (alpha <= 0.0 || beta <= 0.0) {
		return NAN;
	}

	if (alpha < 1.0) {
		u = krandf_r(s);

		return kgamma_r(s, 1.0 + alpha, beta) * pow(u, 1.0 / alpha);
	}

	d = alpha - 1.0 / 3.0;
	c = 1.0 / sqrt(9.0 * d);
	z = knormal_r(s, 0.0, 1.0);
	v = 1.0 + c * z;
	x = v * v * v;
	if (x <= 0.0) {
		return NAN;
	}

	u = krandf_r(s);
	if (u < 1.0 - 0.0331 * z * z * z * z) {
		return beta * d * x;
	}

	return beta * (-log(krandf_r(s)) / d);

}

double
kweibull_r(kstate *s, double lambda, double k)
{
	double u = krandf_r(s);

	return lambda * pow(-log(1.0 - u), 1.0 / k);
}

double
kbeta_r(kstate *s, double alpha, double beta)
{
	double gamma1, gamma2;

	gamma1 = kgamma_r(s, alpha, 1.0);
	gamma2 = kgamma_r(s, beta, 1.0);

	return gamma1 / (gamma1 + gamma2);
}

double
kchisq(int k)
{
	return kchisq_r(&_dsf, k);
}

double
knormal(double mean, double sd)
{
	return knormal_r(&_dsf, mean, sd);
}

double
kexponential(double lambda)
{
	return kexponential_r(&_dsf, lambda);
}

int
kpoisson(double lambda)
{
	return kpoisson_r(&_dsf, lambda);
}

double
kgamma(double alpha, double beta)
{
	return kgamma_r(&_dsf, alpha, beta);
}

double
kweibull(double lambda, double k)
{
	return kweibull_r(&_dsf, lambda, k);
}

double
kbeta(double alpha, double beta)
{
	return kbeta_r(&_dsf, alpha, beta);
}

