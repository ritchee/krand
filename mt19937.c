#include "krand.h"

#define M 397
#define MA 0x9908b0df
#define UMASK 0x80000000
#define LMASK 0x7fffffff

static __thread kmt _mts = {
    .s = {[0] = 0},
    .index = S_LEN + 1,
};

void
kmt_seed_r(kmt *s, u32 seed)
{
    int i;

    s->s[0] = seed & 0xffffffff;
    for (i = 1; i < S_LEN; i++) {
        s->s[i] = (1812433253 * (s->s[i - 1] ^ (s->s[i - 1] >> 30)) + (u32)i);
    }

    s->index = i;
}

u32
kmt_rand_r(kmt *s)
{
    u32 y;
    int i;
    static u32 mag01[2] = {0, MA};

    i = s->index;

    if (i >= S_LEN) {
        int kk;

        if (i == S_LEN + 1)
            kmt_seed_r(s, 5489);

        for (kk = 0; kk < S_LEN - M; kk++) {
            y = (s->s[kk] & UMASK) | (s->s[kk + 1] & LMASK);
            s->s[kk] = s->s[kk + M] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        for (; kk < S_LEN - 1; kk++) {
            y = (s->s[kk] & UMASK) | (s->s[kk + 1] & LMASK);
            s->s[kk] = s->s[kk + (M - S_LEN)] ^ (y >> 1) ^ mag01[y & 0x1];
        }

        y = (s->s[S_LEN - 1] & UMASK) | (s->s[0] & LMASK);
        s->s[S_LEN - 1] = s->s[M - 1] ^ (y >> 1) ^ mag01[y & 0x1];

        i = 0;
    }

    y = s->s[i++];
    s->index = i;

    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680;
    y ^= (y << 15) & 0xefc60000;
    y ^= (y >> 18);

    return y;
}


void
kmt_seed(u32 seed)
{
    kmt_seed_r(&_mts, seed);
}

u32
kmt_rand(void)
{
    return kmt_rand_r(&_mts);
}

