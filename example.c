#include <stdio.h>
#include <time.h>

#include "krand.h"

int
main(void)
{
	kstate s;

	kseed_r(&s, time(NULL) ^ (long)time);
	for (int i = 0; i < 10; i++) {
		printf("%lu\n", krand_r(&s));
	}

	return 0;
}
